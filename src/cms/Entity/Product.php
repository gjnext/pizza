<?php


namespace src\cms\Entity;


use src\System\Config\Config_dev;
use src\System\DataBase\DB;

class Product
{
    /**
     * Product name
     * @var string $name
     */
    public string $name;

    /**
     * Product category
     * @var string $category
     */
    public string $category;

    /**
     * Product description
     * @var string $description
     */
    public string $description;

    /**
     * Product price
     * @var int $price
     */
    public int $price;

    /**
     * Product photo
     * @var array $photos
     */
    public array $photos;

    /**
     * Once product template path
     * @var string $oneProductTemplate
     */
    private string $oneProductTemplate = Config_dev::VIEW . "oneProductTemplate/product.tpl";

    /**
     * Products template path
     * @var string $productsTemplate
     */
    private string $productsTemplate = Config_dev::VIEW . "productsTemplate/products.tpl";

    /**
     * Get product information
     * @param int $category
     * @param int $id
     * @return array
     */
    public function getData(int $category = 1, int $id = 0)
    {
        if ($id != 0) {
            $data = DB::select("
                SELECT `products`.`id`,
                    `category`.`id`,
                    `category`.`description`, 
                    `products`.`name`,
                    `products`.`description`, 
                    `products`.`price` 
                FROM `products`, `category`
                WHERE `products`.`id` = :ID
            ", ["ID" => $id]);
        } else {
            $data = DB::select("
                SELECT `products`.`id`, 
                    `category`.`id`,
                    `category`.`description`,
                    `products`.`name`,
                    `products`.`description`, 
                    `products`.`price` 
                FROM `products`, `category`
                WHERE `products`.`category_id` = :ID
            ", [["ID" => $category]]);
        }
        return $data;
    }

    /**
     * get final product content
     * @param int $count
     * @return string
     */
    public function getProductContent(int $count = 0){
        $content = "";
        if ($count != 0){
            if ($count == 1){
                $content = file_get_contents($this->oneProductTemplate);
                $content = str_replace("{name}", $this->name, $content);
                $content = str_replace("{description}", $this->description, $content);
                $content = str_replace("{price}", $this->price, $content);
            } else {
                $content = file_get_contents($this->productsTemplate);
                $content = str_replace("{name}", $this->name, $content);
                $content = str_replace("{description}", $this->description, $content);
                $content = str_replace("{price}", $this->price, $content);
            }
        }

        return $content;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * @param mixed $photos
     */
    public function setPhotos($photos)
    {
        $this->photos = $photos;
    }

}